mongosamplecreator = require("../mongoconnection")

exports.list = (req, res) ->
  if req.method == 'GET'
      mongosamplecreator.loadMessages((result) ->
        res.json(result)
      )
  else if req.method == 'POST'
      message = req.body
      message.timestamp = new Date().getTime()
      mongosamplecreator.saveMessage(message)
      res.json(message)
  else
      res.json({ method: 'What?' })