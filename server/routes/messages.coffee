mongo = require 'mongodb'
mongoServer = new mongo.Server "10.20.114.96", 27017, {}

client = new mongo.Db 'panelsyncer', mongoServer, {safe:false}

findMessages = (dbErr, collection, res) ->
	console.log "Unable to access database: #{dbErr}" if dbErr
	console.log collection
	collection.find().toArray( (err, items) ->
		if err
			console.log "Unable to find record: #{err}"
		else
			console.log items
			res.json(items)
			client.close()
		)

# GET messages as json
exports.messages = (req, res) ->
	client.open (err, database) ->
		console.log client.collection
		client.collection 'messages', (a, b) => findMessages(a, b, res)


