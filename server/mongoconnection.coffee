mongo = require('mongodb')

mongoServer = new mongo.Server "10.20.114.96", 27017, {}

client = new mongo.Db 'panelsyncer', mongoServer, {safe:false}
client.open (err, database) ->
  console.log "Unable to access database: #{err}" if err
  return

exports.getMongoConnector = () ->
  return client

exports.saveMessage = (message) ->
    client.collection('messages').save message, (err, docs) ->
exports.loadMessages = (resultCallback) ->
  client.collection('messages').find().sort({'timestamp' : -1}).limit(5).toArray((error, results) ->
    resultCallback(results)
  )