express = require 'express'
routes = require './routes'
messages = require './routes/messages'
user = require './routes/user'
list = require './routes/list'
client = require './routes/client'
http = require 'http'
path = require 'path'

app = module.exports = express()

app.configure ->
	app.set 'port', process.env.PORT || 3000
	app.set 'views', __dirname + '/views'
	app.set 'view engine', 'ejs'
	app.use express.favicon()
	app.use express.logger('dev')
	app.use express.bodyParser()
	app.use express.methodOverride()
	app.use app.router
	app.use require('stylus').middleware(__dirname + '/public')
	app.use express.static path.join(__dirname, 'public')

app.configure 'development', ->
	app.use(express.errorHandler())

app.get('/', routes.index)
app.get('/messages', messages.messages)
app.get('/client', client.client)
app.get('/users', user.list)
app.get('/list', list.list)
app.post('/list', list.list)

httpServer = http.createServer app

httpServer.listen app.get('port'), ->
	console.log "Express server listening on port " + app.get('port')

