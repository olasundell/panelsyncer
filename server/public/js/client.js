(function refreshList () {
    $.getJSON('/list', function(data) {
        var items = [];
        console.log(data);
        $.each(data, function(key, item) {
            items.push(
                '<li class="' + item.level + '"> ' +
                    '<span class="icon ' + item.level + '"></span>' +
                    '<div class="message-content">' + item.message +
                    ' <div class="time">(' + new Date(item.timestamp).toDateString() +')</div>' +
                    '</div>' +
                '</li>');
        });

        $('#listWidget ul').empty();
        $('<ul/>', {
            'class': 'my-new-list',
            html: items.join('')
        }).appendTo('#listWidget ul');
    });
    setTimeout(refreshList, 10000);
})();