$.ready(function refreshList () {
    $.getJSON('/list', function(data) {
        var items = [];
        console.log(data);
        $.each(data, function(key, item) {

            items.push('<li>' + JSON.stringify(item) + '</li>');
        });

        $('#listWidget').empty();
        $('<ul/>', {
            'class': 'my-new-list',
            html: items.join('')
        }).appendTo('#listWidget');
    });
    setTimeout(refreshList, 10000);
});