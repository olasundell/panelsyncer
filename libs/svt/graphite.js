var ps = ps || {};
ps.graphite = ps.graphite || {};

ps.graphite.host = 'http://graphite.svti.svt.se:8085'

function Metric(metric, callback) {
    this.continue_poll = true;
    this.metric = metric;
    this.callback = callback;
    this.poll_rate = 1000;
    this.last_time = Math.floor((new Date).getTime() / 1000) - 60000;
}

Metric.prototype.poll = function () {
    var self = this;

    this.fetch_data();

    //this.continue_poll = false;
    if (this.continue_poll) { 
        setTimeout(function(){ self.poll() }, this.poll_rate);
    }
};


Metric.prototype.fetch_data = function () {
    var self = this;
    var req = ps.graphite.host + '/render?format=json'
            + '&target=' + encodeURIComponent("alias(" + self.metric + ",'')")
            + '&from=' + self.last_time;

    d3.json(req, function(json) {
        var points = null;
        if (! json) {
            return self.callback(new Error("unable to load data"));
        }

        points = json[0].datapoints;
        if (points.length == 0) {
            return;
        }

        self.last_time = points[points.length - 1][1]

        return self.callback(null, points);
    });
}





ps.graphite.metric = function (metric, callback) {
    metric = new Metric(metric, callback);
    metric.poll()
};
