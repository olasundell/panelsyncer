Chart = (function () {

	function Chart() {
		this.data = [];
		this.x = 0;
		this.y = 0;
	}

	Chart.prototype.getSeed = function () {
		return seed;
	}

	Chart.prototype.convertData = function(data) {
		var arr = [], l = data.length;
		for (var i = 0 ; i < l; i++ ) {
			arr.push(data[i][0]);
		}
		return arr;
	};

	Chart.prototype.onData = function(error, data) {
		console.log('got data', data);
		var d = this.convertData(data);
		console.log(this);
		if (!this.data.length) {
			this.data = d.slice(0,39);
			this.plot();
		} else {
			this.updateGraph(d);
		}
	};

	Chart.prototype.plot = function() {
		var n = 40, _this = this;

		var margin = {top: 10, right: 10, bottom: 20, left: 40},
				width = 960 - margin.left - margin.right,
				height = 500 - margin.top - margin.bottom;

		this.x = d3.scale.linear()
				.domain([1, n - 2])
				.range([0, width]);

		this.y = d3.scale.linear()
				.domain([0, 30])
				.range([height, 0]);

		this.line = d3.svg.area()
				.interpolate("basis")
				.x(function(d, i) {  return _this.x(i); })
				.y1(height)
				.y0(function(d, i) {  return _this.y(d); });

		var svg = d3.select("body").append("svg")
				.attr("width", width + margin.left + margin.right)
				.attr("height", height + margin.top + margin.bottom)
				.append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		svg.append("defs").append("clipPath")
				.attr("id", "clip")
				.append("rect")
				.attr("width", width)
				.attr("height", height);

		svg.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + height + ")")
				.call(d3.svg.axis().scale(this.x).orient("bottom"));

		svg.append("g")
				.attr("class", "y axis")
				.call(d3.svg.axis().scale(this.y).orient("left"));


		this.path = svg.append("g")
				.attr("clip-path", "url(#clip)")
				.append("path")
				.data([this.data])
				.attr("class", "line")
				.attr("d",this. line);
	};

	Chart.prototype.updateGraph = function(data) {
		var _this = this;

		// push a new data point onto the back
		this.data.push(data);

		// redraw the line, and slide it to the left
		this.path
				.attr("d",this.line)
				.attr("transform", null)
				.transition()
				.duration(500)
				.ease("linear")
				.attr("transform", "translate(" + this.x(0) + ")")
//				.each("end", function(data) {
//					_this.updateGraph(data);
//				});


		// shift the old data point off the front
		for (var i=0; i < data.length; i++) {
			this.data.shift();
		}
	};

	Chart.prototype.load = function () {
		return this.data;
	}

	return Chart;

})();


