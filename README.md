panelsyncer
===========

Syncs alot of panels and presents them nicely.

![cool](https://raw.github.com/olasundell/panelsyncer/master/imgs/cool-business-presentation.jpg "Panel Syncer")
![yeah](https://raw.github.com/olasundell/panelsyncer/master/imgs/farckyeahgraphs.jpg "Panel Syncer")

